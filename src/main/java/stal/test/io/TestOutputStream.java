/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class TestOutputStream {
    
    public static void main(String[] args) throws IOException {
        
        File f = new File("C:\\testdir\\a.txt");
       FileOutputStream fis = new FileOutputStream(f); 
       ObjectOutputStream ois = new ObjectOutputStream(fis);
        
       String str = "lalala";
       
       ois.writeObject(str);
       ois.writeObject(str);
       
       ois.close();
       
    }
    

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class TestInputStream {

  
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        
       File f = new File("C:\\testdir\\a.txt");
       FileInputStream fis = new FileInputStream(f); 
       ObjectInputStream ois = new ObjectInputStream(fis);
     
       
       InputStream is = ois;
       int i = 0;
       while (i>-1) {
       i = is.read();
       char c = (char)i;
          System.out.println(c);
       }
      // fis.close();
  
      
    //  ois.read();
      
    }
  

}

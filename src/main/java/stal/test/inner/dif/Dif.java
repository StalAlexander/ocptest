/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.inner.dif;

import stal.test.inner.AOuter;
import stal.test.inner.AOuter.AInner; // Обязательно!
import stal.test.inner.BOuter;
import stal.test.inner.COuter;

/**
 *
 * @author Stal
 */
public class Dif {
    
    public static void main(String[] args) {
        
        AOuter outer = new AOuter();
        
  //     AInner inner = outer.new AInner(); нельзя если конструктор AInner не public
  //     BInner inner = outer.new BInner(); // нельзя если класс BInner не public
            
          AInner inner = outer.new AInner("as"); // можно, так как и класс и конструктор public 
        
        //   BOuter.StaticInner si = new BOuter.StaticInner(); нельзя если StaticInner AInner не public
          
             COuter.statintC =8;
             
                 //       COuter.StaticInner si = new COuter.StaticInner(); // Нельзя если не private
    }
    
}

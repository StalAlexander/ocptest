/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.inner;

/**
 *
 * @author Stal
 */
public class OuterLocalInner {
    
     Integer defO;
    private Integer privO;
    protected Integer protO;
    public Integer pubO;
    
    static Integer statdefO;
    static private Integer statprivO;
    static protected Integer statprotO;
    static public Integer statpubO;
    
    
// ОШИБКА    Local l = new Local(); // Разумеется, внешний класс не знает о классе внутри метода!
    
    void method(Integer locI, final Integer locJ ){
    
         class Local{  // local class не может  иметь модификаторы доступа ()
          
 // ОШИБКА! local variable locI is accessed from within inner class; needs to be declared final
   //          Integer tt = locI;
               Integer dd = locJ;  // нормально, так как locJ - final
             {
              // имеет доступ ко ВСЕМ полям внешнего класса.
             }
        }
    }
         
 Object getObject(final Integer i){
    
        return new Object(){
            {
             // имеет доступ ко ВСЕМ полям внешнего класса.
            Integer t =  i; // Можно ТОЛЬКО если i - final
 
            }
        
        };
        
    }        
         
    
    
    
}

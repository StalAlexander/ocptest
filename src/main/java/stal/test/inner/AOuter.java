/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.inner;

/**
 *
 * @author Stal
 */
public class AOuter {
    
    Integer defO;
    private Integer privO;
    protected Integer protO;
    public Integer pubO;
    
    static Integer statdefO;
    static private Integer statprivO;
    static protected Integer statprotO;
    static public Integer statpubO;
    
    
   public  AInner inner = new AInner();
  public  AOuter(){
        
       System.out.println("outer");      
    
     
     inner.privI = 7;  // внешний класс имеет доступ ко ВСЕМ полям внутреннего.
     
    }
    
    
    public class AInner{
    
    Integer defI;
    private Integer privI;
    protected Integer protI;
    public Integer pubI;
    
    
    final static int statdefI=0; // можно, так как compile-time constant 
   /*
    
    ОШИБКА!   Inner classes may not declare static members, unless they are compile-time constant fields (§15.28).
    
    static final   static Integer statdefI=0;
    static private Integer statprivI;
    static protected Integer statprotI;
    static public Integer statpubI=0;
  */
    AInner(){
     System.out.println("inner"); 
  //!    AOuter outer = new AOuter();    // Осторожно, рекурсия!   
    };
    
    
      public AInner(String s){
      
         // можно обращаться ко всему как во внешнем так и во внутреннем классе
      
      };
      
      
    void createOuter(){
      AOuter outer = new AOuter();       
     outer.privO = 8;  // внутренний класс имеет доступ ко ВСЕМ полям внешнего.
     }
   
    }
    
     class BInner{
    
    Integer defI;
    private Integer privI;
    protected Integer protI;
    public Integer pubI;
  
    public BInner(String s){};
    
    BInner(){
        
        AInner inner = new AInner(); // из любого внутреннего класса можно видеть любой другой!
        System.out.println("inner"); 
  //!    AOuter outer = new AOuter();    // Осторожно, рекурсия!   
    };
    
    void createOuter(){
      AOuter outer = new AOuter();       
     outer.privO = 8;  // внутренний класс имеет доступ ко ВСЕМ полям внешнего.
     }
   
    }
    
      protected class CInner{
       
       
       }
    
}

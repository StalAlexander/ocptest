/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.serialize;

import java.io.*;

/**
 *
 * @author Alexandr Stal astal@eapo.org; al_stal@mail.ru
 */
public class SObject implements java.io.Serializable {

    static int statI = 777;
    private int pI = 3;
    private Integer pInt = 7;
  transient protected String proStr = "proStr";
    public String pubStr = "pubStr";
 //   Object o = new Object();  //java.io.NotSerializableException: java.lang.Object

    public static void main(String args[]) throws IOException, ClassNotFoundException {
        
        statI = 888;
        FileOutputStream fos = new FileOutputStream("temp.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
      //  SObject ts = new SObject();
       // DObject ds = new DObject();
       
        B ts = new B(7);
        
        ts.a=100;
        ts.b=200;
        
        
        oos.writeObject(ts);
        //oos.writeObject(ds);
        
        oos.flush();
        oos.close();
        
        FileInputStream fis = new FileInputStream("temp.out");
        ObjectInputStream oin = new ObjectInputStream(fis);
        B ts2 = (B) oin.readObject();
      
       // SObject ds2 = (SObject) oin.readObject();
      
        
        System.out.println("a2= " + ts2.a); // static не сериализуется
        System.out.println("b2= " + ts2.b);
    //    System.out.println("b2= " + ts2);
        
        
        
    }

}

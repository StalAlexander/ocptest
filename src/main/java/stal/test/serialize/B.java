/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.serialize;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class B extends A implements java.io.Serializable{
    
    public int b = 9;
    
    public B(int i){    
        super(i);
        b = 10;

        System.out.println("constr B");
    }
    
    public static void main(String[] args) {
        B b = new B(5);
    }

}

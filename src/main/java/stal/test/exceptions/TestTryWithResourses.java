/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.exceptions;

/**
 *
 * @author Alexandr Stal astal@eapo.org; al_stal@mail.ru
 */
public class TestTryWithResourses {

    public static void printSuppressed(Throwable e) {

        System.out.println(e);
        Throwable[] tr = e.getSuppressed();
        System.out.println("Throwable[] tr length = " + tr.length);
        for (Throwable t : tr) {
            System.out.println(t);
        }

    }

    public static void main(String[] args) throws Exception {

        try {
            new TestTryWithResourses().run();
        } catch (Exception e) {
            printSuppressed(e);
            
           
        }
    }

    void run() throws Exception {

        try (Box box = new Box();) {
            System.out.println("Open box");
            throw new Exception("e1");  // подавляется catch
        } catch (Exception e) {

            System.out.println("catch");
            printSuppressed(e);
        //   throw new Exception("e2");  //  Подавляется finally
        } finally {
            System.out.println("finally");
        //    throw new Exception("e finally");

        }

    }

}

class Box implements AutoCloseable {

    @Override
    public void close() throws Exception {
        System.out.println("Close Box");              // срабатывает даже если в try было исключение 
        throw new Exception("e closed");           //  Подавляется finally и catch и try
    }
}

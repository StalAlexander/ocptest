/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.exceptions;

import java.io.IOException;

/**
 *
 * @author Alexandr Stal astal@eapo.org; al_stal@mail.ru
 */
public class  TestRethrow extends TestTryWithResourses {

    void run() throws IOException {
        try {
            throw new IOException();
        } catch (Throwable e) {
            System.out.println(e);
            throw e;
        }
    }

    void flowControl() throws Exception {

        try {

            System.out.println("try");
            throw new Exception("e1");

        } catch (Exception e) {
            
            System.out.println("catch");
            printSuppressed(e);
            Exception e2 = new Exception("e2");
            
            throw e2;  // ПОДАВЛЕНА если finally кинет исключение!
        } finally {
            System.out.println("finally");
            throw new Exception("e3");
        }
    }

    public static void main(String[] args) throws Exception {
        //    new TestRethrow().run();
        try {
            new TestRethrow().flowControl();
        } catch (Exception e) {

            printSuppressed(e);
         

        }

    }

}

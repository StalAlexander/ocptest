package stal.test.base;

/**
 *
 * @author Alexandr Stal astal@eapo.org; al_stal@mail.ru
 */
public class Foo {

    static Foo foo = new Foo();

    {
     // ОШИБКА NPE 
      //  foo.hashCode();
        System.out.println("Foo init");
    }

    
    class A{
    int aa;
    }
    
    class B{
    int bb = new A().aa;
    
    
    }
    
    
    
    
    static {
        //   new Foo();
    //    foo.hashCode();
        System.out.println("Foo static init");
    }

    Foo() {
        System.out.println("Foo constructor");
    }

    public static void main(String[] args) {
        new Foo();
    }

}

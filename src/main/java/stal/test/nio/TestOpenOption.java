/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.nio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import static java.nio.file.StandardOpenOption.*;

/**
 *
 * @author Alexandr Stal astal@eapo.org; al_stal@mail.ru
 */
public class TestOpenOption {

    public static void main(String[] args) throws IOException, InterruptedException {

   //     FileChannel channel = FileChannel.open(Paths.get("C:\\test.txt"), 
     //           StandardOpenOption.READ, StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

    /*    
        StandardOpenOption.APPEND - только в output
        StandardOpenOption.CREATE - если фйла нет - создает. если без этой опции - NSF
        StandardOpenOption.CREATE_NEW  - если фйла нет - создает. Если есть - java.nio.file.FileAlreadyExistsException
        DELETE_ON_CLOSE - удаляет при выходе
        TRUNCATE_EXISTING  - удал.содержимое 
        APPEND + TRUNCATE_EXISTING not allowed
      
    */        
        
        
        OpenOption[] optionsW = new OpenOption[] {CREATE, TRUNCATE_EXISTING, DSYNC };
        OpenOption[] optionsR = new OpenOption[] {  };
        
        Path pathW = Paths.get("C:\\test.txt");
        Path pathR = Paths.get("C:\\test2.txt");
        
        
       InputStream is = Files.newInputStream(pathR, optionsR);  // APPEND not ALLIWED
        
       
     BufferedReader br =  Files.newBufferedReader(pathR, Charset.defaultCharset());
    //   OutputStream os = Files.newOutputStream(path, options);
        
     BufferedWriter bw =  Files.newBufferedWriter(pathW, Charset.defaultCharset(), optionsW);
        
     bw.write("it works!");
       
     Thread.sleep(20*1000);
     bw.write("it works!");
     
     bw.flush();
     bw.close();
     
     
     
        
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.nio;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Stal
 */
public class VisitorTest {
    
    public static void main(String[] args) throws IOException, InterruptedException {
        
        FileVisitor fw = new FileVisitor() {

            @Override
            public FileVisitResult preVisitDirectory(Object dir, BasicFileAttributes attrs) throws IOException {
              return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Object file, BasicFileAttributes attrs) throws IOException {
                
                System.out.println(file);
                
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Object file, IOException exc) throws IOException {
                       return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Object dir, IOException exc) throws IOException {
                        return FileVisitResult.CONTINUE;
            }
        };
        
        
        FileVisitor fw2 = new AA();
        
           Path src = Paths.get("C:\\a");
        
        Files.walkFileTree(src, fw);
        
       WatchService ws =  FileSystems.getDefault().newWatchService();
        src.register(ws, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE);        
      //   WatchKey wk =  ws.take(); //
        WatchKey wk =  src.register(ws, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE);
        src.register(ws, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE);
        
     
        Path pp = Paths.get("C:/aa/bb/../vv/vv/../../dfg.txt");
        
        System.out.println(pp.normalize());
        
  // src.subpath(0, 5);
        /*
         while (true){
         // wk.pollEvents();
        
          wk = ws.poll();
             
          if (wk==null)
              continue;
         List<WatchEvent<?>> lst = wk.pollEvents();
         
         for (WatchEvent event:lst){
             System.out.println(event.kind());
         }
         
             
         }
        */
        /*
        {

            @Override
            public FileVisitResult postVisitDirectory(Object dir, IOException exc) throws IOException {
                return super.postVisitDirectory(dir, exc); //To change body of generated methods, choose Tools | Templates.
            }
         
        } 
        */
        
    }
    
    
   static class AA extends SimpleFileVisitor{
    
    }
}

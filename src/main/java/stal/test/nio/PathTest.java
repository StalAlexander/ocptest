/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.nio.charset.Charset;
import java.nio.file.CopyOption;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.DosFileAttributeView;
import java.nio.file.attribute.FileAttributeView;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.nio.file.attribute.UserPrincipal;

/**
 *
 * @author Stal
 */
public class PathTest {
    
    public static void main(String[] args)  {
        
        /*
        Path path1 = Paths.get("C:\\a\\bb\\c\\d\\e");
        
        System.out.println(path1.getNameCount());
        System.out.println(path1.getClass());
        System.out.println(path1.getName(0));
        System.out.println(path1.getName(1));
        System.out.println(path1.subpath(1,2));
         
        Path p = Paths.get("C:\\a\\bb\\c\\..\\d\\e");
     
        System.out.println(p.subpath(1,4));
        System.out.println(p.normalize());
        
        Path p2 = Paths.get("C:\\a\\bb\\.\\c\\d\\e");
        System.out.println(p2.normalize());
          
        */
        
        Path p3 = Paths.get("C:\\a\\bb\\c\\d\\e");
        Path p4 = Paths.get("C:\\a\\bb\\c\\dd\\ee");
        
        Path p5 = Paths.get("jjj\\kkk");
        Path p6 = Paths.get("mmm\\nnn");   
        
        System.out.println(p3.resolve(p4));
     //     System.out.println(p4.resolve(p3));
        
  //      System.out.println(p3.resolve(p5));
       // p3.res
        System.out.println(p5.resolve(p3));      
        System.out.println(p5.resolve(""));      
        
        /*
        
        resolve
        Абс1(Абс2) ->Абс2
        Абс1() ->Абс1
        Абс1(Отн1)->Абс1+Отн1
        Отн1(Абс2) ->Абс2
        Отн1()->Отн1
        */
        
      //  System.out.println(p3.resolveSibling(p5));   
        
        
        /*
        resolveSibling
        X(Абс2) ->Абс2
        X(Отн) = (X-1)+Отн
        */
        
     //   System.out.println(p5.resolve(""));      
        
        
          System.out.println(p3.relativize(p4));      
        
          /*
           X(Y) =  от X перейти к Y
          */
        
        System.out.println(p3.getRoot());
    
        FileSystem fs = FileSystems.getDefault();
        
        Path src = Paths.get("C:\\a\\aaa.txt");
        Path tgt = Paths.get("C:\\a\\bbb.txt");
       Path tgt2 = Paths.get("C:\\a\\ccc.txt");
       Path tgt3 = Paths.get("C:\\a\\dir\\zzz.txt"); 
       Path tgt4 = Paths.get("C:\\a\\dir2\\zzz.txt"); 

       try{
      //  Files.copy(src, tgt, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES  );  //Возможно java.nio.file.NoSuchFileException java.nio.file.FileAlreadyExistsException
        
    //   Files.delete(tgt);  // java.nio.file.NoSuchFileException java.nio.file.AccessDeniedException
 //  Files.delete(tgt2);  // java.nio.file.NoSuchFileException java.nio.file.AccessDeniedException

 //  Files.deleteIfExists(tgt3); //java.nio.file.DirectoryNotEmptyException
   //Files.move(tgt3, tgt4);
     
        //  System.out.println("isDirectory:" + Files.getAttribute(tgt3, "isDirectory"));
       
        //   Class<String> str = String.class;
        BasicFileAttributes bf =   Files.readAttributes(tgt3, BasicFileAttributes.class, LinkOption.NOFOLLOW_LINKS);
           
        
       UserDefinedFileAttributeView fw = Files.getFileAttributeView(tgt3, UserDefinedFileAttributeView.class);

       PosixFileAttributeView pf = null;
       
     
       
     fw.write("DFGH",Charset.defaultCharset().encode("true") );
       
       
           System.out.println(fw.name());
           
           
       } catch(IOException e){
            
          System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
          System.out.println(e);
          System.out.println(e.getMessage());
            
        }
        
       
        
        
      //  System.out.println(;
    
    }
    
    interface AAA extends CopyOption{
    
    }
}

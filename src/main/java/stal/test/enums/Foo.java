/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.enums;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class Foo {

    
    Foo(){System.out.println("constr");}
    
    static  Foo f;
    {
        System.out.println("init");
     f.hashCode();
    }
    
    static {
        System.out.println("static init");
     // f.hashCode();
    }
    
    static final Foo F = new Foo();
    
    public static void main(String[] args) {
    //    System.out.println("Foo");  
      Singleton s =  Singleton.INSTANCE;
      s.cc(); // можно
     
    Foo d = new  Foo();
      
     //  Singleton s2 = new Singleton(); Ошибка! Нельзя вызвать конструктор enum'а извне!
      
    }
    
}

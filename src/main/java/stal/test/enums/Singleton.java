/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.enums;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public enum Singleton {
    
    INSTANCE;

    static int tt;
    static{
      INSTANCE.aa(); // можно!
    }
    
    {
   
      //  tt =5;
      // Ошибка  tt = 45;
     // ОШИБКА! INSTANCE.aa();
        // Причем если бы это был не 
    }
    
    
    static int bb;
    protected void cc(){};
    private void aa(){};
 
    static  void statAA(){
    INSTANCE.aa();  // можно!
    }
    
    
 //   public Singleton(int i){} // Ошибка! Нельзя объявить public конструктор enum
    
     Singleton(){
     //  this("");  // 
     }
    Singleton(String str){
      this();  // Можно (так как внутри enum)!
        
    }
    
}

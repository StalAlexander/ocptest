/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.generic;

/**
 *
 * @author stal
 */
public class Dif {
 
    public static void main(Object... obj){
    
        Ageneric<String> a = new Ageneric<>();
        Ageneric a2 = new Ageneric();
        
        a2.set(new Object());
        a.set("");
        
        
        Bgeneric b = new Bgeneric();      
        b.set(b);
        
  //Error! not generic!
   //     Bgeneric<Integer> b2 = new Bgeneric<Integer>();      
   //     b.set(b);
      
       Cgeneric<Integer> c = new Cgeneric<>() ;  // корректно! 
       
       MethogGeneric mg = new MethogGeneric();
       
       mg.method(c);
       
       Ageneric<?> aaa = new Ageneric<>();
       
     // НЕЛЬЗЯ ПЕРЕДАВАТЬ арг-ы в wildcard  aaa.set(new A());
       
     //  Ageneric<? super A> asu = new Ageneric<>();
       
       
    }
    
}

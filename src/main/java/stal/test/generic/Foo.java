/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.generic;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */


public class Foo {

    public static void main(String[] args) {
        
        SimpleGeneric<String> sg = new SimpleGeneric<>();
        
        SimpleGeneric ss = new SimpleGeneric<>();

   // Нельзя!     SimpleGeneric<A> sa = new SimpleGeneric<B>();
   
   // Нельзя    SimpleGeneric<B> sb = new SimpleGeneric<A>();
  

     // Нельзя!   ExtGeneric<String> st = new ExtGeneric<>();
        
        ExtGeneric<A> st = new ExtGeneric<A>();
        
        ExtGeneric<B> stb = new ExtGeneric<B>();
        
        IntGeneric i = new IntGeneric();
     //   IntGeneric<A> ia = new IntGeneric<>(); // A не реализует Simple Interface       
      //  B b = new B();
           IntGeneric<B> ia = new IntGeneric<>();  //  корректно
        
    }
    
}


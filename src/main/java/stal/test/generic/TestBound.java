/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.generic;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class TestBound {

    
    static void m(Ageneric<? super B> a){};
    
    static <T> T mmm(T t){return t;} 
    
    public static void main(String[] args) {
        
        Ageneric<?> ga = new Ageneric<>();
        
        // Нельзя! a.set(new Object());
        Object ob = ga.get();
        
        Ageneric<? extends A> ga3 = new Ageneric<>();
        // Нельзя! ga3.set(new A());
        // Нельзя! ga3.set(new Object());
        A aa = ga3.get();
        
        Ageneric<? super A> ga2 = new Ageneric<>();
        ga2.set(new A());
        // Нельзя! ga2.set(new Object());
        // Нельзя! A a = ga2.get();     
        Object a = ga2.get();
        
        
         Ageneric<?> ga4 = new Ageneric<A>();
         
         Ageneric<?> ga5 = new Ageneric();
         
      //   ga5.set(a);
         
         Ageneric<? super A> ga6 = new Ageneric<A>();
    
         Ageneric<? super A> ga7 = new Ageneric<Object>();
    
         Ageneric<? super B> ga8 = new Ageneric<A>();
           
         Ageneric<? extends A> ga9 = new Ageneric<B>();
      
        m(ga7);
        
           Ageneric ga10 = new Ageneric<A>();
    
           Ageneric ga12 = new Ageneric<String>();
           
            Ageneric<Double> ga13 = new Ageneric<Double>();
           
            
            ga10.set(Double.NaN);
            

            
           ga10.set(a);
           ga10.set(new Object());
           
           
           ga12 = ga10;
          ga12=ga10; 
                     
           Ageneric<A> ga11;
           ga11 = ga10;
    
           ga12.get();
           
  //        A aaa =  ga11.get();
           
           
           
    //     <Integer>mmm(new Integer(7));  
      
           //TestBound tb;
           
           TestBound.<Integer>mmm(9);
           
           mmm(56);
           
    }
    
    
}

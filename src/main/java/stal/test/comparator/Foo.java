/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.comparator;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class Foo {
    
    static class A<AA extends Person>{
    
        AA a;
    }
    

    public static void main(String[] args) {
       // Map map = new TreeMap();
        
        Set set = new TreeSet();
        /*
        set.add(7);
        set.add(8);
      //  set.add("edxwex"); //java.lang.Integer cannot be cast to java.lang.String
        */
      //   set.add("A");  //java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Integer
      //   set.add(7);
         
     //   set.add(new Object()); // java.lang.Object cannot be cast to java.lang.Comparable
      //  set.add(new Object());
      //  set.add(null); //java.lang.NullPointerException
        
        set.add(new Person(1,33,"A"));
       // set.add(new Person(200,12,"B"));
     //   set.add(new Object());
        
        
        System.out.println(set);
       
        
          TreeSet tree = new TreeSet(new Comparator<Object>(){

          
            @Override
            public int compare(Object o1, Object o2) {
                  return o1.hashCode()-o2.hashCode();
            }
          
          });

          tree.add(new Person(1, 23, "AAA"));
          tree.add(new Person(2, 1200000000, "BBB"));
          tree.add(new Object());
          
       //  Comparator c =  tree.comparator()
        System.out.println(tree);
      
        //  TreeSet<String> treeStr = new TreeSet<>(new MyComparator<Object>());  // если компаратор возвращает 0 то записывается только 1й элемент, т.к. для попадания используется не эквалс а компаре
       
        TreeSet<String> treeStr = new TreeSet<>();
          treeStr.add("S");
          treeStr.add("D");
          treeStr.add("A");
          treeStr.add("a");
          treeStr.add("aA");
          treeStr.add("AA");
          treeStr.add("aa");
          treeStr.add("1");    
          
          System.out.println(treeStr);
          
          
    }
          
    
}

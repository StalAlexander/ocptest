/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.comparator;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class Person  implements Comparable<Object>{

    int id;
    int age;
    String str;

    public Person(int id, int age, String str) {
        this.id = id;
        this.age = age;
        this.str = str;
    }

    @Override
    public String toString() {
        return  str; //super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
          
        return age;
          //  return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
     }

    
    
    /*
    @Override
    public int compareTo(Person o) {
        return id-o.id;
    }
    */

    @Override
    public int compareTo(Object o) {
      return hashCode()-o.hashCode();
    }
}

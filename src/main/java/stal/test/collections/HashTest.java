/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class HashTest {

    static class MyObject{
       
        Integer id;
        String str;
        
        
        
/*
        @Override
        public int hashCode() {
              return id;
        }
 */   

        public MyObject(Integer id, String str) {
            this.id = id;
            this.str = str;
        }

        @Override
        public String toString() {
            return str; 
        }

        @Override
        public boolean equals(Object obj) {
                   if (obj instanceof MyObject){
                     //  return id == ((MyObject)obj).id;
                     //  return false; 
                   }
                      
                   
                   return false;
                       
        }

        @Override
        public int hashCode() {
 
            
           // int hash = 7;
           // hash = 53 * hash + Objects.hashCode(this.id);
           // return hash;
            return super.hashCode();
        }
 
        
        }
    
    public static void main(String[] args) {
        
        List<Object> list = new ArrayList();
        
        list.add(new MyObject(0,"A"));
        list.add(new MyObject(0,"B"));
        
        list.remove(new MyObject(0, "C"));  // проверка по equals
   //     System.out.println(list);
        
        
        Set set = new HashSet();
        
        set.add(new MyObject(0,"A"));
        set.add(new MyObject(0,"B")); // добавляется, так как не переопределен хэш-код. Если х.к. переопределен то не добавляется
        
     //   System.out.println(set);  
       
        
          List<Object> list2 = new LinkedList();
        //  List<Object> list2 = new ArrayList();
        
          
          list2.add(new MyObject(0,"A"));
          list2.add(new MyObject(0,"B"));
          
          list2.remove(new MyObject(0,"B"));
          
        System.out.println(list2);  
      
        
          System.out.println(new MyObject(0,"A").hashCode());
          
          System.out.println(new MyObject(0,"A").hashCode());
          System.out.println(new MyObject(1,"BB").hashCode());
          
          
          
          
          
        
    }
    
    
}

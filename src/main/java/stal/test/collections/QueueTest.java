/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.collections;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class QueueTest {

    public static void main(String[] args) {
        Queue que = new ArrayDeque();
    
     //   que.add("A");
      //  que.add("B");
        System.out.println(que.peek());
        System.out.println(que.peek()); // возвращает 1й но не удаляет его
      //  System.out.println(que.poll());
      //  System.out.println(que.poll());
    //    que.remove();   NoSuchElementException если нечего удалять
   //     System.out.println(que.remove()); // удаляемый эл-т.
        System.out.println(que.poll()); // null если нечего удалять
        
        System.out.println(que.element());//  NoSuchElementException если нечего возвращать
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Stal
 */
public class TestSet {
    
    static class AAA{
    String str;

        static int i = 10;
        public AAA(String str) {
            this.str = str;
        }

        @Override
        public int hashCode() {
          //  i--;
            return i;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final AAA other = (AAA) obj;
            if (!Objects.equals(this.str, other.str)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return str;        }
    
      
    
    }
    
    public static void main(String[] args) {
        
        List list = new ArrayList();
        list.add("A");
        list.add("A");
        list.add("A");
        
        System.out.println(list);
        
        Set s = new HashSet(list);
        
        System.out.println(s);
        
           Set s2 = new TreeSet(list);
        
    //    s2.add(new Object());  // java.lang.ClassCastException: java.lang.Object cannot be cast to java.lang.Comparable
        
          Set s3 = new TreeSet(list);
        
     //   s3.add(new Object());  // java.lang.ClassCastException: java.lang.Object cannot be cast to java.lang.Comparable
     //   s3.add(new Object());
        
        
          Set s4 = new HashSet();
        
          s4.add(new AAA("A"));
          s4.add(new AAA("A"));
          s4.add(new AAA("A"));
          
          
          
           System.out.println(s4);
         
          
        
    }
    
}

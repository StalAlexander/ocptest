/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.collections;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author Alexandr Stal astal@eapo.org; al_stal@mail.ru
 */
public class MapTest {

    public static void main(String[] args) {

        Map map = new HashMap(); // TreeMap();
        map.put(null, null); // NPE для TreeMap, Можно для остальных

        NavigableMap<String, String> nm = new TreeMap<>();
        nm.put("B", "BB");
        nm.put("C", "CC");
        nm.put("A", "AA");
        nm.put("D", "DD");
        nm.put("E", "EE");

        System.out.println(nm);

        SortedMap mapTail = nm.tailMap("C"); // оставляет хвост с указанного элемента ВКЛЮЧИТЕЛЬНО
        //     SortedMap mapTail = nm.tailMap("C", false); // оставляет хвост с указанного элемента НЕ включая его самого

        System.out.println(mapTail);

   //      mapTail.remove("C");    // tailMap не создает копию! удаление в возвращенном Map = удаление в исходном!
        System.out.println(mapTail);

        System.out.println(nm);

        SortedMap sumMap = nm.subMap("B", false, "D", false); // не включая краев
        // SortedMap sumMap = nm.subMap("B", "D"); //  включая края
        //c //  java.lang.IllegalArgumentException !!!!!!!!!!!

       //  SortedMap sumMap = nm.subMap(null, "B"); // java.lang.NullPointerException!!!!!!!!!1
        System.out.println(sumMap);

        SortedMap headMap = nm.headMap("B"); // оставляет голову с указанного элемента НЕ включая его самого

      //  headMap = nm.headMap("B", true); // оставляет голову с указанного элемента  включая его самого

        System.out.println(headMap);
        
      //  NavigableMap navi;
        
        Map map2 = new LinkedHashMap();
        

    }

}

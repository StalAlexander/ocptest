/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.collections;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;

/**
 *
 * @author Stal
 */
public class TestDeque {

    public static void main(String[] args) {

        Deque d = new LinkedList();//  ArrayDeque(2);

        System.out.println(d.add("A"));

        /*
         System.out.println(d.offer("A"));
         System.out.println(d.offer("B"));
         System.out.println(d.offer("C"));
         System.out.println(d.offer("D")); 
         */
        System.out.println(d.peek()); // не удаляет

        System.out.println(d);

        System.out.println(d.pop()); //  удаляет

        System.out.println(d);


  //      System.out.println(d.pop()); //java.util.NoSuchElementException

        System.out.println(d.add("B"));
                 
        System.out.println(d.remove()); //  удаляет
        
        System.out.println(d);
     
      //     System.out.println(d.remove()); //java.util.NoSuchElementException
  
        System.out.println(d.add("C"));
  
        System.out.println(d.poll()); // удаляет
        
        System.out.println(d);
     
        System.out.println(d.poll()); //  null
       
       
        System.out.println(d.add("D"));
  
        System.out.println(d.peek()); // НЕ удаляет
        
        System.out.println(d);
        d.remove();
     
        System.out.println(d.peek()); //  null
       
        
        System.out.println(d.add("E"));
        
        System.out.println(d.element()); // НЕ удаляет
           
        d.remove();
     
     //   System.out.println(d.element()); //  //java.util.NoSuchElementException    
        
        d.add("F"); // в хвост
        d.push("G"); // в голову
        d.add("H"); // в хвост
        d.offer("J"); // в хвост
        System.out.println(d);
        
        
        
    }

}

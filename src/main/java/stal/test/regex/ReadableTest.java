/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.regex;

import java.io.IOException;
import java.nio.CharBuffer;
import java.util.Scanner;

/**
 *
 * @author Alexandr Stal astal@eapo.org; al_stal@mail.ru
 */
public class ReadableTest {

    public static void main(String[] args) {

        Readable r = new Readable() {

            @Override
            public int read(CharBuffer cb) throws IOException {
                return 1;
            }

        };

      //  Scanner s = new Scanner(r);
        String str = "1 3 45 a 46";

        Scanner s = new Scanner(str);
     //   s.useDelimiter("girl");
        while (s.hasNextInt()) {
            System.out.println(s.nextInt());
        }

    }

}

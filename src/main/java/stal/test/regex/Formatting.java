/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.regex;

import java.util.Formatter;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class Formatting {

    public static void main(String[] args) {
        
   //     System.out.printf("My name %s I live in %s  "); // java.util.MissingFormatArgumentException
      //    System.out.printf("My name %s I live in %s  ", "Sasha", "Moscow"); // java.util.MissingFormatArgumentException
        
      //    System.out.printf("My name %s I live in %s  ", 123, "Moscow"); // java.util.MissingFormatArgumentException
     //       System.out.printf("My name %s I live in %s  ", new Object(), "Moscow"); // java.util.MissingFormatArgumentException
       
         //      System.out.printf("My name %s I live in %s  ", new Object(), "Moscow"); // java.util.MissingFormatArgumentException
          //   System.out.printf("My name %s I'm %d years old  ", "Sasha", 123); // java.util.MissingFormatArgumentException
       
     //      System.out.printf("My name %s I'm %d years old  ", "Sasha", "123"); // java.util.MissingFormatArgumentException  //java.util.IllegalFormatConversionException
       
        //      System.out.printf("My name %s I'm %b   ", "Sasha", 123); 
       //      System.out.printf("My name %s I'm%10b   ", "Sasha", 123); 
    //     System.out.printf("My name %s I'm %+1.3b   ", "Sasha", 123);  //Conversion = b, Flags = +
     //      System.out.printf("My name %s I'm %-1.3b   ", "Sasha", 123);  //Conversion = b, Flags = +
             
      //       System.out.printf("My name %s I'm %0100.1d years old \n", "Sasha", 123.349);  //Exception in thread "main" java.util.IllegalFormatPrecisionException: 1
       
            //   System.out.printf("My name %s I'm %0100d years old \n", "Sasha", 123);  //Exception in thread "main" java.util.IllegalFormatPrecisionException: 1
       
           System.out.printf("My name %s I'm %+,(-10d years old \n", "Sasha", 36);  //Exception in thread "main" java.util.IllegalFormatPrecisionException: 1
       
     /*   
        Formatter f = new Formatter();
        
        f.format("AAA %s %b" , "dddd", 1);
       String str = "";
       */
        
     //   System.out.fo (f.out());
        
        
        
    }
    
    
}

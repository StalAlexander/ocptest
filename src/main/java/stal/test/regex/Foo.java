/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class Foo {
    
    public static void main(String[] args) {
        
        /*
        String regex = "d+";
        String target = "I loved another women...";  //Group = d starts at6 ends at 7
        */
      
        /* Все
        String regex = "d?";
        String target = "I loved another women...";  //Group = d starts at6 ends at 7
       */
        
        /*
       //  dlove starts at 6 ends at 11
        String regex = "d+\\w*";
        String target = "I lovedlove another women...";  //Group = d starts at6 ends at 7
       */
        
        /*
        String regex = "d+[\\s\\S]*"; // до конца строки
        String target = "I lovedlove another women...";  //Group = d starts at6 ends at 7
        */
   
        /*
        String regex = "((loved)|(hate)){1}\\w*"; // Group = lovedloved starts at 2 ends at 12 Group = hate starts at 40 ends at 44
        String target = "I lovedloved another women... and now I hate";  //Group = d starts at6 ends at 7
        */
        
        
        String regex = ".*lo{1}"; // Group = lovedloved starts at 2 ends at 12 Group = hate starts at 40 ends at 44
        String target = "I lovedlovedloloved another women... !!and now I hate";  //Group = d starts at6 ends at 7
        
        
        
        Pattern pattern = Pattern.compile(regex);
        
        Matcher matcher = pattern.matcher(target);
        
        while(matcher.find()){
            System.out.println("Group = " + matcher.group()+ " starts at " + matcher.start() + " ends at " + matcher.end() );
        }
        
        String str = "cat cup copp";
        String newString = str.replaceFirst("c.p\\b", "()"); //line4
        System.out.println(newString);
        
      
        
        
    }
    

}

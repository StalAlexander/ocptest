/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.thread;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Stal
 */
public class Tennis {

    public static void main(String[] args) {

        TBall ball = new TBall();
        Player p1 = new Player("vasya", ball);
        Player p2 = new Player("petya", ball);

        Thread t1 = new Thread(p1);
        Thread t2 = new Thread(p2);

        t1.start();
        t2.start();

        System.out.println("111");

    }

    static class TBall {
    };

    static class Player implements Runnable {

        String name;
        TBall ball;

        public Player(String name, TBall ball) {
            this.name = name;
            this.ball = ball;
        }

        @Override
        public void run() {

            while (true) {
                synchronized (ball) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println(name + " send ball");
                }
            }
        }

    }

};

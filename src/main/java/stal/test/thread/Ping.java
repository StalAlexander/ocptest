/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.thread;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Stal
 */
public class Ping {
    
    
    public static void main(String[] args) {
      
        
    Ball b = new Ball();
    Thread t1 = new Thread(new Player1(b), "player1");
    Thread t2 = new Thread(new Player2(b), "player2");
    
    
    t1.start();
    t2.start();
        
        
    }
    
    
   static class Player1 implements Runnable
    {
       Ball ball;
       public Player1(Ball ball){
       this.ball = ball;
       }
       
        @Override
        public void run() {
          
            while (true){
                try {

                    ball.sayPing();
                    
                } catch (InterruptedException ex) {
                    Logger.getLogger(Ping.class.getName()).log(Level.SEVERE, null, ex);
                }  
            }
                
            
        }
    
    };
   
   
      static class Player2 implements Runnable
    {
       Ball ball;
       public Player2(Ball ball){
       this.ball = ball;
       }
       
        @Override
        public void run() {
          
            while (true){
                try {
         
                    ball.sayPong();
                    
                } catch (InterruptedException ex) {
                    Logger.getLogger(Ping.class.getName()).log(Level.SEVERE, null, ex);
                }  
            }
                
            
        }
    
    };
   
   
   static class Ball{
   
       int count = 0;
   synchronized void sayPing() throws InterruptedException{
       Thread.sleep(100);
         count++;
           System.out.println("Ping " + count);
       }
       
       
       
     synchronized  void sayPong() throws InterruptedException{
       Thread.sleep(100);
        count--;
           System.out.println("Pong " + count);
       }
       
   }
   
     
       
   
   
    
}

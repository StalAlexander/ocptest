/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.concurrent;

import java.util.concurrent.Exchanger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class ExchangerTest {

    public static void main(String[] args) {
        
       
        
        Exchanger<String> exchanger = new Exchanger<>();
        
        Thread t1 = new Thread(new C(exchanger, "AAA", 2000, "RUB"));
        Thread t2 = new Thread(new C(exchanger, "BBB", 4000, "USD"));
        
        t1.start();t2.start();
        
    }
    
}


class C implements Runnable{
 final Exchanger<String> exchanger;
 final String name;
 final long waitTime;
 final String cash;
    public C(Exchanger exchanger, String name, long waitTime,String cash ) {
        this.exchanger = exchanger;
        this.name = name;
        this.waitTime = waitTime;
        this.cash = cash;
        
    }

    @Override
    public void run() {
           
     try {
         System.out.println(name + " is working...");
         Thread.sleep(waitTime);
         System.out.println(name + " is ready for change !" + cash);
         exchanger.exchange(cash);
         
         
         System.out.println(name + " got " + exchanger.exchange(cash));
     
     } catch (InterruptedException ex) {
         Logger.getLogger(C.class.getName()).log(Level.SEVERE, null, ex);
     }
        
    
    }
 
 

}

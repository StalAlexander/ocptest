/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class FutureTest {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        
          ExecutorService service = Executors.newFixedThreadPool(5);
          
         
          Future f = service.submit(new F("AAA"));
          
         Future<Integer> f2 = service.submit(new J<Integer>(200,1000));
         Future<Integer> f3 = service.submit(new J<Integer>(400,5000));
          
         service.shutdown();
          
          System.out.println(f2.get()+f3.get());
          
          
        
    }
    
}

class F implements  Runnable{
String name;

    public F(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        
        
           System.out.println(name);
         //  throw new NullPointerException();
    }
}


class J<T> implements Callable{

    T t;
    int millis;

    public J(T t, int millis) {
        this.t = t;
        this.millis = millis;
    }
    
    
    
    @Override
    public Object call() throws Exception {
          System.out.println(t);
          Thread.sleep(millis);
          return t;
          
    }

}
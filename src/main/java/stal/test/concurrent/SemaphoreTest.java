/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.concurrent;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class SemaphoreTest {

    public static void main(String[] args) {
        
      Semaphore semA = new Semaphore(1);
      Semaphore semB = new Semaphore(0);
      
      Thread t1 = new Thread(new A(semA, semB, "AAA"));
      Thread t2 = new Thread(new A(semB, semA, "BBB"));
      t1.start(); t2.start();
    
    }
    
}

class A implements Runnable{
private final Semaphore semFirst;
private final Semaphore semSecomd;

private final String name;

    public A(Semaphore semA,Semaphore semB, String name) {
        this.semFirst = semA;
        this.semSecomd = semB;
        this.name = name;
    }

    @Override
    public void run() {
   
        
        
     while(true){   
        try {
        semFirst.acquire();
        System.out.println(name);
        semSecomd.release();
            
    } catch (InterruptedException ex) {
        Logger.getLogger(A.class.getName()).log(Level.SEVERE, null, ex);
    }
     }
    
    }

}

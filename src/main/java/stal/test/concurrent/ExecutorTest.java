/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package stal.test.concurrent;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class ExecutorTest {
    
    public static void main(String[] args) {
      
        ExecutorService service = Executors.newFixedThreadPool(5);
        
        

        service.execute(new D(5,"AAA"));
        service.execute(new D(5,"BBB"));
        service.execute(new D(5,"CCC"));
        service.execute(new D(5,"DDD"));
        service.execute(new D(5,"EEE"));
        
        service.shutdown();
        
        
        
        
    }
    

}

class D implements Runnable{
private int count;
private String name;

    public D(int count, String name) {
        this.count = count;
        this.name = name;
    }

    @Override
    public void run() {
          for (int i = 0; i<count; i++){
              try {
                  Thread.sleep(500);
              } catch (InterruptedException ex) {
                  Logger.getLogger(D.class.getName()).log(Level.SEVERE, null, ex);
              }
              System.out.println(name + " " + i);
          }

    }



}





/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.concurrent;

import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class CountDownLatchTest {

    public static void main(String[] args) throws InterruptedException {
        java.util.concurrent.CountDownLatch latch = new CountDownLatch(3);
        
      Thread t1 = new Thread(new B(latch, "BOOM!"));
      Thread t2 = new Thread(new B(latch,"BAM!"));
      t1.start();
      t2.start();
      
      for (int i = 0; i<10; i++){
          Thread.sleep(i);
          System.out.println("count =  " + i);
          latch.countDown();
      }
      
        
    }
    
}


class B implements Runnable{
final private CountDownLatch latch; 
final private String name;

    public B(CountDownLatch latch, String name) {
        this.latch = latch;
        this.name = name;
    }

    @Override
    public void run() {
    try {
        latch.await();
        System.out.println(name);
        
    } catch (InterruptedException ex) {
        Logger.getLogger(B.class.getName()).log(Level.SEVERE, null, ex);
    }
        
    }




}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stal.test.concurrent;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Stal
 */
public class QueueTest {
    
    public static void main(String[] args) {

      
        BlockingQueue q = new  ArrayBlockingQueue<>(5);
        q.add(1);
        q.add(2);
        q.add(3);
        
        
     /*         
      Thread t1 = new Thread(new AQ(q,"AQ",2000));
      Thread t2 = new Thread(new BQ(q,"BQ",3000));
      t1.start();
      t2.start();
      */
      Thread t1 = new Thread(new AQ(q,"AQ",3000));
      Thread t2 = new Thread(new BQ(q,"BQ",2000));
      t1.start();
      t2.start();
        
        
    }
    
}


class AQ implements Runnable{
    BlockingQueue q;
    String name; 
    int delta;

    public AQ(BlockingQueue q, String name, int delta) {
        this.q = q;
        this.name = name;
        this.delta = delta;
    }

    @Override
    public void run() {
        while(true){
            try {
                Thread.sleep(delta);
            } catch (InterruptedException ex) {
                Logger.getLogger(AQ.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                System.out.println("take " +  q.take());
            } catch (InterruptedException ex) {
                Logger.getLogger(AQ.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}

class BQ implements Runnable{
    BlockingQueue q;
    String name; 
    int delta;

    int i = 100;
    
    public BQ(BlockingQueue q, String name, int delta) {
        this.q = q;
        this.name = name;
        this.delta = delta;
    }

    @Override
    public void run() {
        while(true){
            try {
                Thread.sleep(delta);
            } catch (InterruptedException ex) {
                Logger.getLogger(AQ.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            System.out.println("put = " + ++i);
            try {
                q.put(i);
            } catch (InterruptedException ex) {
                Logger.getLogger(BQ.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    
    

}



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.sorting;

import java.util.Arrays;
import java.util.Comparator;
import stal.test.comparator.Person;

/**
 *
 * @author Alexandr Stal
 * astal@eapo.org; al_stal@mail.ru
 */
public class ASort {

    static class SuperPerson extends Person{

        public SuperPerson(int id, int age, String str) {
            super(id, age, str);
        }
    };
    
    public static void main(String[] args) {
        
        int[] au = {1000,1,56,3,89,3,44,0,1};
        
    //    Arrays.sort(au);
        
        Arrays.sort(au,1,6);
        
        
        for(int i:au){
            System.out.println(i);
        }
        
        Person p1 = new Person(1,10,"A");
        Person p2 = new Person(2,20,"AB");
        
    //    Object[] ob = {p1,p2, new Integer(1)};  в  binarySearch   // java.lang.ClassCastException: stal.test.comparator.Person cannot be cast to java.lang.Integer
         Object[] ob = {p1,p2}; 
        /*
        Arrays.sort(ob, new Comparator<Person>(){

            @Override
            public int compare(Person o1, Person o2) {
                    return o1.hashCode()-o2.hashCode();
            }
        });
        */
   //     Arrays.sort(ob);   java.lang.Object cannot be cast to java.lang.Comparable
        /*
        Arrays.sort(ob, null);
        
        for(Object i:ob){
            System.out.println(i);
        }
        */
          Person p3 = new Person(2,10000,"AB");
          
        System.out.println(Arrays.binarySearch(ob, p3));  // java.lang.ClassCastException: stal.test.comparator.Person cannot be cast to java.lang.Integer
        
    }
    
    
}
